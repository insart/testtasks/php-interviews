<?php

class ParentClass {
    public static function whoAmI() {
        echo "ParentClass\n";
    }

    public static function callStatic() {
        static::whoAmI();
    }
}

class ChildClass extends ParentClass {
    public static function whoAmI() {
        echo "ChildClass\n";
    }
}

ChildClass::callStatic(); // Що Виведе?

ParentClass::callStatic(); // Що Виведе?
